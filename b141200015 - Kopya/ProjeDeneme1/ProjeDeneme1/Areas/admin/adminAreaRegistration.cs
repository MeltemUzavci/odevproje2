﻿using System.Web.Mvc;

namespace ProjeDeneme1.Areas.admin
{
    public class adminAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "admin";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "admin_default",
                "admin/{controller}/{action}/{id}",
                new {controller="Home", action = "AdminEkrani", id = UrlParameter.Optional }
               

            );
        }
    }
}